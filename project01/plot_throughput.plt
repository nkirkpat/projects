set terminal png
set output 'throughput.png'

set style data histogram
set style histogram cluster gap 1

set xlabel 'File Size'
set ylabel 'Throughput (MB/s)'

#set style fill solid border rgb "black"
set auto x
set yrange [0:*]
plot 'throughput.txt' using 2:xtic(1) title col, \
        '' using 3:xtic(1) title col, \
        '' using 4:xtic(1) title col, \
        '' using 5:xtic(1) title col, \
        '' using 6:xtic(1) title col

