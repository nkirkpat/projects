set terminal png
set output 'latency.png'

set style data histogram
set style histogram cluster gap 1

set xlabel 'Server Mode'
set ylabel 'Average request time (s)'

#set style fill solid border rgb "black"
set auto x
set yrange [0:*]
plot 'latency.txt' using 2:xtic(1) title col, \
        '' using 3:xtic(1) title col, \
        '' using 4:xtic(1) title col, \
        '' using 5:xtic(1) title col, \
        '' using 6:xtic(1) title col

