#!/usr/bin/env python2.7

import mimetypes
import socket
import logging
import fnmatch
import re
import errno
import getopt
import os
import sys
import time
import signal

# Globals

ADDRESS = '0.0.0.0'
BACKLOG  = 0
LOGLEVEL = logging.DEBUG
PROGRAM  = os.path.basename(sys.argv[0])

DOCROOT = "."
PORT = 9234
FORKING = False
VERBOSE = False

# Usage function

def usage(status=0):
    print '''Usage: spidey.py [-d DOCROOT -p PORT -f -v]

Options:

    -h         Show this help message
    -f         Enable forking mode
    -v         Set logging to DEBUG level

    -d DOCROOT Set root directory (default is current directory)
    -p PORT    TCP Port to listen to (default is 9234)'''.format(os.path.basename(sys.argv[0]))
    sys.exit(status)

# BaseHandler Class - copied from echo_server.py
class BaseHandler(object):
    def __init__(self, fd, address):
        ''' Construct handler from file descriptor and remote client address '''
        self.logger  = logging.getLogger()        # Grab logging instance
        self.socket  = fd                         # Store socket file descriptor
        self.address = '{}:{}'.format(*address)   # Store address
        self.stream  = self.socket.makefile('w+') # Open file object from file descriptor
        self.debug('Connect')

    def debug(self, message, *args):
        ''' Convenience debugging function '''
        message = message.format(*args)
        self.logger.debug('{} | {}'.format(self.address, message))

    def info(self, message, *args):
        ''' Convenience information function '''
        message = message.format(*args)
        self.logger.info('{} | {}'.format(self.address, message))

    def warn(self, message, *args):
        ''' Convenience warning function '''
        message = message.format(*args)
        self.logger.warn('{} | {}'.format(self.address, message))

    def error(self, message, *args):
        ''' Convenience error function '''
        message = message.format(*args)
        self.logger.error('{} | {}'.format(self.address, message))

    def exception(self, message, *args):
        ''' Convenience exception function '''
        message = message.format(*args)
        self.logger.exception('{} | {}'.format(self.address, message))

    def handle(self):
        ''' Handle connection '''
        self.debug('Handle')
        raise NotImplementedError

    def finish(self):
        ''' Finish connection by flushing stream, shutting down socket, and
        then closing it '''
        self.debug('Finish')
        try:
            self.stream.flush()
            self.socket.shutdown(socket.SHUT_RDWR)
        except socket.error as e:
            pass    # Ignore socket errors
        finally:
            self.socket.close()

# HTTPHandler class
class HTTPHandler(BaseHandler):
      def __init__(self, fd, address, docroot=None):
            BaseHandler.__init__(self, fd, address)
            self.docroot = os.path.abspath(DOCROOT)
      def _parse_request(self):
          # Set REMOTE_ADDR from address
          os.environ['REMOTE_ADDR'] = self.address.split(':',1)[0]
          os.environ['REMOTE_HOST'] = self.address.split(':',1)[0]
          os.environ['REMOTE_PORT'] = self.address.split(':',1)[1]
          
          # Read stream and set REQUEST_METHOD
          data = self.stream.readline().strip().split()
          self.debug('Parsing  {}'.format(data))
          if '?' in data[1]:
              os.environ['QUERY_STRING'] = data[1][(data[1].index('?')+1):]
              os.environ['REQUEST_URI'] = data[1][:data[1].index('?')]
          else:
              os.environ['QUERY_STRING'] = ''
              os.environ['REQUEST_URI'] = data[1]
          os.environ['REQUEST_METHOD'] = data[0]
          
          data = self.stream.readline().rstrip()
          while data:
              line = data.split(':',1)
              variable = 'HTTP_'+line[0].replace('-','_')
              variable = variable.upper()
              os.environ[variable] = line[1].strip()
              data = self.stream.readline().rstrip()

      def handle(self):
          # Parse HTTP request and headers
          self._parse_request()

          # Build uripath by normalizing REQUEST_URI
          self.uripath = os.path.normpath(self.docroot + os.environ['REQUEST_URI'])
          # Check path existence and types and then dispatch
          
          # if path doesnt exist or path doesnt start with self.docroot
          if not os.path.exists(self.uripath) or not self.uripath.startswith(self.docroot):
              self._handle_error(404) # 404 error
          #if path is a file and executable
          elif os.path.isfile(self.uripath) and os.access(self.uripath,os.X_OK):
              self._handle_script()   # CGI script
          #if path is a file and readable
          elif os.path.isfile(self.uripath) and os.access(self.uripath,os.R_OK):
              self._handle_file()     # Static file
          # if path is a directory and readable
          elif os.path.isdir(self.uripath) and os.access(self.uripath,os.R_OK):
              self._handle_directory()# Directory listing
          else:
              self._handler_error(403)# 403 error

      def _handle_file(self):
          self.debug('Handling File')
          mimetype = mimetypes.guess_type(self.uripath)
          if mimetype is None:
              mimetype = ['application/octet-stream','']
          self.stream.write('HTTP/1.0 200 OK\r\n')
          self.stream.write('Content-Type: '+mimetype[0]+'\r\n')
          self.stream.write('\r\n')
          for line in open(self.uripath):
              self.stream.write(line)
          self.stream.flush()
           
      def _handle_directory(self):
          self.debug('Handling Directory')
          #construct an HTML page that lists the contents of the directory.
          self.stream.write('HTTP/1.0 200 OK\r\n')
          self.stream.write('Content-Type: text/html\r\n')
          self.stream.write('\r\n')
          self.stream.write('<h1>Directory Listing: {}/</h1>'.format(self.uripath))
          folderData = []
          fileData = []
          tableRowHeader = ['Type','Name','Size']
          for root, dirs, files in os.walk(self.uripath):
              for d in sorted(dirs):
                  path=root+'/'+d
                  row = ['Directory',str(d),os.path.getsize(path)]
                  folderData.append(row)
              for f in sorted(files):
                  path = root+'/'+f
                  row = ['File',str(f),os.path.getsize(path)]
                  fileData.append(row)
              break
          htmlcode = '<table>'
          htmlcode = htmlcode + '<tr>'
          for i in range(0,len(tableRowHeader)):
              htmlcode = htmlcode + '<th>'+tableRowHeader[i]+'</th>'
          htmlcode = htmlcode + '</tr>'
          for directory in folderData:
              htmlcode = htmlcode + '<tr>'
              htmlcode = htmlcode + '<td>{}</td>'.format(directory[0])
              htmlcode = htmlcode + '<td>'
              htmlcode = htmlcode + '<a href={}>{}</a>'.format(os.path.join(os.environ['REQUEST_URI'], directory[1]),str(directory[1]))
              htmlcode = htmlcode + '</td>'
              htmlcode = htmlcode + '<td>{}</td>'.format(str(directory[2]))
              htmlcode = htmlcode + '</tr>'
          for f in fileData:
              htmlcode = htmlcode + '<tr>'
              htmlcode = htmlcode + '<td>{}</td>'.format(f[0])
              htmlcode = htmlcode + '<td>'
              htmlcode = htmlcode + '<a href={}>{}</a>'.format(os.path.join(os.environ['REQUEST_URI'], f[1]),str(f[1]))
              htmlcode = htmlcode + '</td>'
              htmlcode = htmlcode + '<td>{}</td>'.format(str(f[2]))
              htmlcode = htmlcode + '</tr>'
          htmlcode = htmlcode + '</table>'
          self.stream.write(htmlcode)
          
      def _handle_script(self):
          signal.signal(signal.SIGCHLD,signal.SIG_DFL)
          for line in os.popen(self.uripath):
              self.stream.write(line)
          signal.signal(signal.SIGCHLD,signal.SIG_IGN)
          """#need to use os.popen command to execute the script and stream the output of the command to the socket
          """
          
      def _handle_error(self,error):
          #construct an HTML page that displays the specified error code. Also insert a jovial picture for visitors amusement.
          self.stream.write('<h1>{}</h1>'.format(error))
          self.stream.write('<img src="http://img.memecdn.com/morphius-404_o_1938383.jpg"/>')
          
# TCPServer Class - copied from echo_server.py (from pbui)

class TCPServer(object):

    def __init__(self, address=ADDRESS, port=PORT, handler=HTTPHandler):
        ''' Construct TCPServer object with the specified address, port, and
        handler '''
        self.logger  = logging.getLogger()                              # Grab logging instance
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
        self.address = address                                          # Store address to listen on
        self.port    = PORT                                             # Store port to lisen on
        self.handler = handler                                          # Store handler for incoming connections

    def process_connection(self,client,address):
        try:
            handler = self.handler(client, address)
            handler.handle()
        except Exception as e:
            handler.exception('Exception: {}', e)
        finally:
            handler.finish()

    def run(self):
        ''' Run TCP Server on specified address and port by calling the
        specified handler on each incoming connection '''
        try:
            # Bind socket to address and port and then listen
            self.socket.bind((self.address, self.port))
            self.socket.listen(BACKLOG)
        except socket.error as e:
            self.logger.error('Could not listen on {}:{}: {}'.format(self.address, self.port, e))
            sys.exit(1)

        self.logger.info('Listening on {}:{}...'.format(self.address, self.port))

        while True:
            # Accept incoming connection
            client, address = self.socket.accept()
            if not FORKING:
                self.logger.debug('Accepted connection from {}:{}'.format(*address))
                self.process_connection(client,address)
            else:
                pid = os.fork()
                if pid == 0: # Child processes client connection
                    self.logger.debug('Accepted connection from {}:{}'.format(*address))
                    self.process_connection(client,address)
                    os._exit(0)
                else:
                    client.close()

# Main execution

if __name__ == '__main__':
    # Parse command-line arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hfvd:p:")
    except getopt.GetoptError as e:
        print e
        usage(1)
    
    for o, a in opts:
        if o == '-h':
            usage(0)
        elif o == '-f':
            FORKING = True
        elif o == '-v':
            VERBOSE = True
        elif o == '-d':
            DOCROOT = a
        elif o == '-p':
            PORT = int(a)
        else:
            usage(1)

    # Set Logging Level        
    logging.basicConfig(
        level   = LOGLEVEL,
        format  = '[%(asctime)s] %(message)s',
        datefmt = '%Y-%m-%d %H:%M:%S',
    )
    # Instantiate and run server
    server = TCPServer('0.0.0.0',port=PORT,handler=HTTPHandler)
    
    try:
        server.run()
    except KeyboardInterrupt:
        sys.exit(0)
