#!/usr/bin/env python2.7
#This is a simple http client that hammers a server with requests
#and gets responses

import getopt
import logging
import os
import socket
import sys
import re
from urlparse import urlparse
import time
from utilities import parseURL

#CONSTANTS
PROCESSES = 1
REQUESTS = 1
VERBOSE = False
PORT = 80

LOGGER = logging.getLogger()
logging.basicConfig(format='[%(asctime)s] %(message)s')

#Utility Functions
def usage(status=0):
    print '''Usage: thor.py [-r REQUESTS -p PROCESSES -v] URL

Options:

    -h           Show this help message
    -v           Set logging to DEBUG level

    -r REQUESTS  Number of requests per process (default is 1)
    -p PROCESSES Number of processes (default is 1)'''.format(os.path.basename(sys.argv[0]))
    sys.exit(status)

try:
    opts, args = getopt.getopt(sys.argv[1:], 'hvr:p:')
except getopt.GetoptError as e:
    print e
    usage(1)

for o, a in opts:
    if o == '-h':
        usage(0)
    if o == '-v':
        VERBOSE = True
    if o == '-r':
        REQUESTS = int(a)
    if o == '-p':
        PROCESSES = int(a)
URL=sys.argv[-1] #URL = last argument of the command


#TCP Client Class -- straight from bui
class TCPClient(object):

    def __init__(self, address=URL, port=PORT):
        ''' Construct TCPClient object with the specified address and port '''
        self.logger  = logging.getLogger()                              # Grab logging instance
        logging.basicConfig(format='[%(asctime)s] %(message)s')
        if VERBOSE:
            self.logger.setLevel(logging.DEBUG)
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
        self.address = address                                          # Store address to listen on
        self.port    = PORT                                             # Store port to lisen on

    def handle(self):
        ''' Handle connection '''
        self.logger.debug('Not Implemented Handle Method')
        raise NotImplementedError

    def run(self):
        ''' Run client by connecting to specified address and port and then
        executing the handle method '''
        try:
            # Connect to server with specified address and port, create file object
            self.start = time.time()
            self.socket.connect((self.address, self.port))
            self.stream = self.socket.makefile('w+')
        except socket.error as e:
            self.logger.error('Could not connect to {}:{}: {}'.format(self.address, self.port, e))
            sys.exit(1)
        if VERBOSE:
            self.logger.debug('Connected to {}:{}...'.format(self.address, self.port))

        # Run handle method and then the finish method
        try:
            self.handle()
        except Exception as e:
            self.logger.exception('Exception: {}', e)
        finally:
            self.finish()

    def finish(self):
        ''' Finish connection '''
        if VERBOSE:
            self.logger.debug('FINISH')
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except socket.error:
            pass    # Ignore socket errors
        finally:
            self.socket.close()
            end = time.time()
            #print end, self.start
            self.elapsed = end - self.start
            #if VERBOSE:
             #   self.logger.debug('{} | Elapsed Time {} seconds'.format(os.getpid(),round(self.elapsed,3)))

#Http Client class
class HTTPClient(TCPClient):
    ######
    #NEEDS WORK PARSING URLS
    #SPECIFICALLY ADDRESSES THAT END WITH /
    #AND URLS THAT START WITH HTTPS://
    def __init__(self, url):
        TCPClient.__init__(self, None, None) # Initialize base class
        #parsing the url
        url_info = parseURL(URL)
        self.host = url_info['hostname']
        if 'path' in url_info:
            self.path = url_info['path']
        else:
            self.path = '/'
        if 'port' in url_info:
            self.port = url_info['port']
        
        if VERBOSE:
            self.logger.debug('URL: {}'.format(URL))
            self.logger.debug('HOST: {}'.format(self.host))
            self.logger.debug('PATH: {}'.format(self.path))
            self.logger.debug('PORT: {}'.format(self.port))
        self.address = socket.gethostbyname(self.host)
        #print self.address
        #print self.port

    def handle(self):
        ''' Handle connection by reading data and then writing it back until EOF '''
        if VERBOSE:
            self.logger.debug('handle...')
        try:
            with open('./request.txt', 'r') as content_file:
                data = content_file.read()
                data = data.replace('www.example.com',self.host)
                data = data.replace('/index.html',self.path)
                #send request to server
                if VERBOSE:
                    self.logger.debug('sending request')
                self.stream.write(data)
                self.stream.flush()

                # Read from Server to STDOUT
                if VERBOSE:
                    self.logger.debug('receiving response')
                data = self.stream.read()
                sys.stdout.write(data)

        except socket.error:
            pass    # Ignore socket errors

def run_processes(processes, requests):
    if VERBOSE:
            LOGGER.setLevel(logging.DEBUG)
    pid = -1
    for x in range(0,processes):
        start = time.time()
        pid = os.fork()

        if pid == 0:
            proc_start = time.time()
            sum = 0
            for x in range(0,requests):
                client = HTTPClient(URL)
                client.run()
                sum += client.elapsed
            avg = round(sum/requests,3)
            LOGGER.debug('{} | Average elapsed time: {}'.format(os.getpid(),avg))
            proc_end = time.time()
            LOGGER.debug('{} | Elapsed time: {}'.format(os.getpid(),round(proc_end-proc_start,3)))
            sys.exit()
    if pid != 0:
        for x in range(0,processes):
            status = os.wait()
            #LOGGER.debug('Process {} exited with status {}'.format(status[0],status[1]))
if __name__ == '__main__':
    run_processes(PROCESSES,REQUESTS)
