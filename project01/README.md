Project 01: Networking
======================

Group Members:
==============
- Anthony Narin: anarin
- Nick Kirkpatrick: nkirkpat

Files:
======
- README: the file you are reading to learn about the other files
- spidey.py: the server side program for activity 2.
  $ ./spidey.py [-d DOCROOT -p PORT -f -v]
- thor.py: the client side program for activity 1.
  $ ./thor.py [-r REQUESTS -p PROCESSES -v] URL
- latency.sh: shell script for measuring the average latency of spidey.py in single connection vs forking mode.
  $ ./latency.sh
- Makefile: start the server, run the experiments, generate the plots, and generate the pdf file
  $ make
- report.tex: the outline by which pdflatex generates the pdf file
  $ pdflatex report.tex

[networking project]: https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/homework09.html
