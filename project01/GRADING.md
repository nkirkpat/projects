Project 01 - Grading
====================

**Score**: 19 / 20

Deductions
----------

* Thor

    - 0.25  Doesn't handle no arguments gracefully
    - 0.5   Missing error checking of system calls (fork)

* Spidey

    - 0.25  Missing error status code

* Report

    *       Report trails off at the end...

Comments
--------
