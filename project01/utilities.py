#! /usr/bin/python
#Utility function for parsing url's
import sys

def parseURL(url):
    split_by_slash = url.split('/')
    if 'http:' in split_by_slash:
        split_by_slash=split_by_slash[1:]

    split_by_slash = filter(bool,split_by_slash)
    path = ''
    
    hostname_port = split_by_slash[0]
    port = -1
    if ':' in hostname_port:
        l = hostname_port.split(':')
        port = int(l[1])
        hostname = l[0]
    else:
        hostname = hostname_port
    if len(split_by_slash) > 1:
        path_list = split_by_slash[1:]
        for string in path_list:
            path += '/'+string

    values = {}
    values['hostname'] = hostname
    if port != -1:
        values['port'] = port
    if path != '':
        values['path'] = path
    return values
    

if __name__ == '__main__':
   print( parseURL(sys.argv[-1]))
