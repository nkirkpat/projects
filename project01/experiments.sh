#! /bin/sh

while getopts f:n:h: name
do
    case $name in
	f)fport=$OPTARG;;
	n)nport=$OPTARG;;
	h)host=$OPTARG;;
        *)echo "Invalid arg";;
	esac
done

if [ -z $nport ]; then
    nport=9000
fi
if [ -z $fport ]; then
    fport=9001
fi
if [ -z $host ]; then
    host='student00.cse.nd.edu'
fi

#This runs thor to get avg time on spidey
./spidey.py -p $nport 2>&1 > /dev/null &

#run without forking mode
./thor.py -v -p 3 -r 3 $host:$nport 2>&1 > /dev/null | grep Average >> tmp.txt
./thor.py -v -p 3 -r 3 $host:$nport 2>&1 > /dev/null | grep Average >> tmp.txt
./thor.py -v -p 3 -r 3 $host:$nport 2>&1 > /dev/null | grep Average >> tmp.txt

awk 'BEGIN{ORS=" ";print "Not-Forked: "}{ print $(NF)} ' tmp.txt > latency_regular.txt
#RUN SERVER WITH FORKING REPEAT ABOVE COMMANDS

rm tmp.txt

./spidey.py -p $fport -f 2>&1 > /dev/null &

./thor.py -v -p 3 -r 3 $host:$fport 2>&1 > /dev/null | grep Average > tmp.txt
./thor.py -v -p 3 -r 3 $host:$fport 2>&1 > /dev/null | grep Average >> tmp.txt
./thor.py -v -p 3 -r 3 $host:$fport 2>&1 > /dev/null | grep Average >> tmp.txt

awk 'BEGIN{ORS=" ";print "\nForked: "}{ print $(NF)} ' tmp.txt > latency_forked.txt

echo 'Mode 1 2 3 4 5 6'>latency.txt
cat latency_regular.txt >> latency.txt
cat latency_forked.txt >> latency.txt
rm tmp.txt

##################################################################
##########Calculating throughput##################################
#Create random files
if [ ! -e gbfile ]; then
    dd bs=1024 count=1048576 </dev/urandom > ./gbfile
fi
if [ ! -e mbfile ]; then
    dd bs=1024 count=1024 </dev/urandom > ./mbfile
fi
if [ ! -e kbfile ]; then
    dd bs=1024 count=1 </dev/urandom > ./kbfile
fi

#through put = size/time
echo 'calculating throughput'
./thor.py -v -p 3 -r 3 $host:$nport/kbfile 2>&1 > /dev/null | grep Average > tmp.txt
./thor.py -v -p 3 -r 3 $host:$nport/kbfile 2>&1 > /dev/null | grep Average >> tmp.txt

awk 'BEGIN{ORS=" ";print "KB: "}{ print .00098/$(NF)} ' tmp.txt > KB.txt

./thor.py -v -p 3 -r 3 $host:$nport/mbfile 2>&1 > /dev/null | grep Average > tmp.txt
./thor.py -v -p 3 -r 3 $host:$nport/mbfile 2>&1 > /dev/null | grep Average >> tmp.txt

awk 'BEGIN{ORS=" ";print "\nMB: "}{ print 1/$(NF)} ' tmp.txt > MB.txt

./thor.py -v -p 3 -r 3 $host:$nport/gbfile 2>&1 > /dev/null | grep Average > tmp.txt
./thor.py -v -p 3 -r 3 $host:$nport/gbfile 2>&1 > /dev/null | grep Average >> tmp.txt

awk 'BEGIN{ORS=" ";print "\nGB: "}{ print 1024/$(NF)} ' tmp.txt > GB.txt


echo 'Mode  1   2   3   4    5   6'>throughput.txt
cat KB.txt >> throughput.txt
cat MB.txt >> throughput.txt
cat GB.txt >> throughput.txt
rm tmp.txt
rm KB.txt
rm GB.txt
rm MB.txt