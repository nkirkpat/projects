#!/usr/bin/env python2.7

#This is the hulk program that brute forces MD5 hashes
import getopt
import string
import hashlib
from itertools import product
import sys

def usage(status=0):
    print'''Usage: hulk.py [-a ALPHABET -l LENGTH -s HASHES -p PREFIX]

Options:

      -a  ALPHABET    Alphabet used for passwords
      -l  LENGTH      Length for passwords
      -s  HASHES      Path to file containing hashes
      -p  PREFIX      Prefix to use for each candidate password'''.format(os.path.basename(sys.argv[0]))
    sys.exit(status)

#CONSTANTS
ALPHABET = 'abcdefghijklmnopqrstuvwxyz0123456789'
LENGTH = 8
HASH_FILE = './hashes.txt'
PREFIX = ''

try:
    opts, args = getopt.getopt(sys.argv[1:], 'a:l:s:p:h')
except getopt.GetoptError as e:
    print e
    usage(1)

for o, a in opts:
    if o == '-h':
        usage(0)
    if o == '-a':
        ALPHABET = a
    if o == '-s':
        HASH_FILE = a
    if o == '-p':
        PREFIX = a
    if o == '-l':
        LENGTH = int(a)
#returns md5 sum of s
def get_md5sum(s):
    return hashlib.md5(s).hexdigest()

def get_hashes(permutations):
    d = {}
    for perm in permutations:
        h = get_md5sum(perm)
        d[h] = perm

    return d

def check_file(d,file_path):
    with open(file_path) as f:
        to_return = []
        for line in f:
            if line.strip() in d:
                to_return.append(d[line.strip()])
        
        return set(to_return)
#Main Execution
if __name__ == '__main__':
    all_permutations = [PREFIX + ''.join(perm) for perm in product(ALPHABET,repeat=LENGTH)]
    #stores hashes as keys and cracked string as value 
    STRING_HASH = get_hashes(all_permutations)
    passwords = check_file(STRING_HASH,HASH_FILE)
    print '\n'.join(passwords)
