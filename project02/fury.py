#!/usr/bin/env python2.7

import os
import sys
import work_queue
import json
import string
from itertools import product
import urllib

# Download hashes.txt if it doesnt exist

# Constants

HASH_FILE = 'hashes.txt'
if not os.path.isfile(HASH_FILE):
    urllib.urlretrieve ("http://yld.me/btd?raw=1", HASH_FILE)

try:
    JOURNAL = json.load(open('journal.json'))
except Exception as e:
    JOURNAL = {}

ALPHABET = 'abcdefghijklmnopqrstuvwxyz0123456789'
perms3 = product(ALPHABET,repeat=3)
perms2 = product(ALPHABET,repeat=2)
perms1 = product(ALPHABET,repeat=1)

comm1 = './hulk.py -s '+HASH_FILE+' -l '
comm = './hulk.py -s '+HASH_FILE+' -l 5 -p '
commandList = [comm1+'1',comm1+'2',comm1+'3',comm1+'4',comm1+'5']

for p in perms1:
    commandList.append(comm+''.join(p))
for p in perms2:
    commandList.append(comm+''.join(p))
for p in perms3:
    commandList.append(comm+''.join(p))

# Main Execution

if __name__ == '__main__':
    # Create Work Queue master with:
    # 1. Random port between 9000 - 9999
    # 2. Project name of hulk-NETID
    # 3. Catalog mode enabled
    queue = work_queue.WorkQueue(work_queue.WORK_QUEUE_RANDOM_PORT, name='hulk-nkirkpat', catalog=True)
    queue.specify_log('fury.log') # Specify Work Queue log location

    for command in commandList:
        task    = work_queue.Task(command)  # Create Work Queue task
        # Add source files
        for source in ('hulk.py', HASH_FILE):
            task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)
        # Submit tasks with journal check
        if command not in JOURNAL:
            queue.submit(task)
        else:
            print >>sys.stderr, 'Already did',command

    while not queue.empty():
        # Wait for a task to complete
        task = queue.wait()
        # Check if task is valid and if task returned successfully
        if task and task.return_status == 0:
            print 'finished {}'.format(task.command)
            JOURNAL[task.command] = task.output.split()
            with open('journal.json.new','w') as stream:
                json.dump(JOURNAL,stream)
            os.rename('journal.json.new','journal.json')            
            # Write output of task to stdout
            # sys.stdout.write(task.output)
            # sys.stdout.flush()
