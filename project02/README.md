Project 02: Distributed Computing
=================================

Please see the [distributed computing project] write-up.

[distributed computing project]: https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/homework10.html

Activity 1: Hulk
================

1) The implementation of Hulk.py works as follows:
   It accepts the required command line arguments, and then a method that accepts the prefix, the alphabet, and the password length creates a list of all the possible plaintext passwords. It does this by recursively going through the alphabet string. The implementation is from the python website. Once it has this list of possible plain text passwords, it finds the md5 hash of those words and puts them into a dictionary where the hash is the key and the plain text is the value. Lastly, a function takes that dictionary and checks it against the values listed in the hash file. Any hashes that match hulk prints the corresponding plaintext value. Hulk has now 'cracked' those passwords. 
   I verified that it worked properly with the test commands and test file in the assignment, and then I changed the commands slightly to check all of the command line options were working correctly.

Activity 2: Fury
================

2) Describe the implementation of fury.py. How does it:
   a) Utilize hulk.py to crack passwords?
      fury.py utilizes a Work Queue to coordinate multiple workers running hulk.py on different machines. After setting up the correct environment variables, we create a master (i.e. fury.py) who creates a queue with a given project name. Once created, we submit various tasks to be completed for the workers. The tasks are simply calls to hulk.py, but running it on multiple machines with different work that was divided up on fury.py. It is important to note that you cant assume the machines had access to the hulk.py and hashes.txt files that would be used for the job. That meant you had to add the source files to the task before submitting the it on the queue.
      Simply running './fury.py' would do nothing but submit the tasks that and wait for workers to connect and complete them. However, the master does not create the workers, so we had to do that separately using the following commands:
      $ work_queue_worker -d all -N hulk-nkirkpat
      This command would only create a single worker to that would try to connect to your project name.
      $ condor_submit_workers -N hulk-nkirkpat 200
      This was the command that would create a cluster of workers on your project. Your project would now have 200 workers that would pick up a task, run it, and then return after completion and wait for their next assignment.

   b) Divide up the work among the different workers?
      The work was divided up in the following manner. The first couple of commands were run by simply calling './hulk.py -l n -s hashes2.txt'. 'n' being 1-5 because the jobs are not too difficult if the length is below 5. However, once the length exceeded 5, the work could not simply be './hulk.py -l 6 -s hashes2.txt'. Instead, I got the permutations of the prefix length 1, and then created 36 different taks with the command './hulk.py -l 5 -s hashes2.txt -p PREFIX' where PREFIX was every possible permutation of length 1-3. The total number of tasks was somewhere in the upper 40,000s which makes sense because of the possible permutaions of length 3.

   c) Keep track of what has been attempted?
      In order to keep track of what work had been done, we used a journal. Using JSON formatting for the data into a dictionary with the key being the command called, and the value being the resulting passwords that were found using hulk.py. In the case of a failure, which will be discussed in part d, we would not submit the task if it already came to completion with an exit status of 0. 
      We also had the ability to watch the work being completed using a script made by professor Bui. If you called '$ ~pbui/pub/bin/work_queue_monitor fury.log', you could watch the status of the fury.log which was being updated during runtime.

   d) Recover from failures?
      In order to account for both data integrity in the off chance of an early program termination, I would create a temporary journal.json file named 'journal.json.new', once the data was completed, only then did I replace the old file with this temporary file by overwriting the old journal.json file with the command 'os.rename('journal.json.new', 'journal.json')'. By doing this technique, we ensured that if a write operation was interrupted, we would not be left with a corrupt or incomplete journal.

   In order to show that fury.py worked properly, I initially set up the queue to only submit one task with the command './hulk.py -l 1 -s hashes.txt'. Once calling './fury.py' we created a single worker using the command listed above in part (a). fury.py would print the stdout from the task. If that output matched the output from simply calling './hulk.py -l 1 -s hashes.txt', then we knew it was working. 

3) From your experience in this project and recalling your Discrete Math knowledge, what would make a password more difficult to brute-force: more complex alphabet or longer password? Explain.
   2 factors influence the difficulty of brute forcing passwords. The first is the length of the password, and the second is the complexity of the alphabet. Determining which of them has more of an influence depends entirely upon which one creates more password possibilities. The equation for the number of possible passwords from an alphabet of length A and a length of L can be expressed as A!/(A-L)! which turns into a sequence of P(A,L) = A*A-1*A-2*A-3...*A-L. To determine which factor is more important lets look at this sequence. If one were to increase the alphabet by X then each number in the sequence would also increase by X but, the number of terms in the sequence remains the same. If one were to increase the length of the password by X, each term in the sequence would maintain the same value, but the number of terms would increase by X. This means that the result of P(A,L) grows significantly faster as L increases than if A increases.

Activiy 3: Deadpool
===================

After completing hulk.py and fury.py, we decrypted as many passwords as we could. Once we got to at least 8000 passwords (in order to attain full credit), we submitted our passwords to the location.

$ cat passwords.txt | curl --data-binary @- http://xavier.h4x0r.space:9111/nkirkpat