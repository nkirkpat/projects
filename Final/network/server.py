#Server class that listens on a given port
#Waits for client to connect
#moves client connection to another available port

from twisted.internet.protocol import ClientFactory
from twisted.internet.protocol import Protocol
from twisted.internet import reactor
from twisted.internet.defer import DeferredQueue

from CommandConn import CommandConnFactory

COMMAND_PORT = 40079


if __name__ == '__main__':
    clientConnections = []
    commandConnFactory = CommandConnFactory({})
    port = reactor.listenTCP(COMMAND_PORT,commandConnFactory)
    commandConnFactory.port = port
    reactor.run()
