from twisted.internet.protocol import ClientFactory
from twisted.internet.protocol import Protocol
from twisted.internet import reactor
from twisted.internet.defer import DeferredQueue

DATA_CONNECTIONS = {}

class DataConnFactory(ClientFactory):
    def __init__(self,commandConn):
        self.commandConn = commandConn
        self.partner_connection = None

    def buildProtocol(self,addr):
        return DataConn(addr,self.commandConn)

class DataConn(Protocol):
    def __init__(self,addr,commandConn):
        self.commandConn = commandConn
        self.addr = addr
    def connectionMade(self):
        print 'data connection made'
        
    def dataReceived(self,data):
        print data
        global DATA_CONNECTIONS
        split = data.split(":")
        if split[0] == 'portnum':
            DATA_CONNECTIONS[split[1]] = self
            self.portnum = split[1]

            #print DATA_CONNECTIONS

            #Find partner connection
            partner_portnum = self.commandConn.getPartner(self.portnum)
            if partner_portnum is not None:
                self.partner_connection = DATA_CONNECTIONS[partner_portnum]
                self.partner_connection.setPartner(self)
                self.transport.write('red')

        else:
            self.partner_connection.transport.write(data)

    def connectionLost(self,reason):
        #remove port from list of client connections
        self.commandConn.removeConnection(self.portnum)
        print 'removed', self.portnum

    def setPartner(self,dataConn):
        self.partner_connection = dataConn
        self.transport.write('blue')
