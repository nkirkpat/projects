from twisted.internet.protocol import ClientFactory
from twisted.internet.protocol import Protocol
from twisted.internet import reactor
from twisted.internet.defer import DeferredQueue

import hashlib
import time,os

HOST = 'student00.cse.nd.edu'
PORT = 40079

class ClientConnFactory(ClientFactory):
    def __init__(self,f):
        self.f = f

    def buildProtocol(self,addr):
        return ClientConn(self.f)

class ClientConn(Protocol):
    def __init__(self,f):
        self.f = f

    def connectionMade(self):
        print 'made command connection'

    def dataReceived(self,data):
        split = data.split(':')
        self.dataPort = int(split[1])
        self.transport.loseConnection()

    def connectionLost(self,reason):
        reactor.connectTCP(HOST,self.dataPort,ClientDataFactory(self.dataPort,self.f))

class ClientDataFactory(ClientFactory):
    def __init__(self,dataPort,f):
        self.dataPort = dataPort
        self.f = f
    def buildProtocol(self,addr):
        return ClientDataConn(self.dataPort,self.f)

class ClientDataConn(Protocol):
    def __init__(self,dataPort,f):
        self.dataPort = dataPort
        self.f = f
        
    def connectionMade(self):
        #print made data connection
        self.transport.write('portnum:'+str(self.dataPort)+':portnum')

    def connectionLost(self,reason):
        print 'data connection ended'

    def dataReceived(self,data):
        print 'CLIENT - data received:',data
        if (data == 'blue' or data=='red'):
            with open(self.f,'w+') as f:
                f.write(data)
            if data == 'red':
                self.monitor_file()
        else:
            print data
            with open(self.f,'w+') as f:
                f.write(data)
            self.monitor_file()
            

    def sendData(self,data):
        self.transport.write(data)

    def monitor_file(self):
        print 'monitoring file'             
        orig =  os.stat(self.f)[8]

        while True:
            m_time = os.stat(self.f)[8]
            if m_time > orig:
                print 'CLIENT:monitor_file - file changed'
                with open(self.f,'r') as f:
                    line = f.read()
                    self.transport.write(line)
                    print 'CLIENT:monitor_file - wrote',line
                orig = m_time
                break
            else:
                time.sleep(1)
            
        
if __name__ == '__main__':
    f = open('test_1.txt','w+')
    reactor.connectTCP(HOST,PORT,ClientConnFactory('test_1.txt'))
    reactor.run()
