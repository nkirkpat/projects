from twisted.internet.protocol import ClientFactory
from twisted.internet.protocol import Protocol
from twisted.internet import reactor
from twisted.internet.defer import DeferredQueue
import traceback
from DataConn import DataConnFactory

COMMAND_PORT = 40079
CLIENT_CONNECTIONS = {}

class CommandConnFactory(ClientFactory):
    def __init__(self,clientConnections):
        self.clientConnections = clientConnections
        self.port = None

    def buildProtocol(self,addr):
        return CommandConn(addr,self.clientConnections,self.port)

    def setPort(self,port):
        self.port = port

class CommandConn(Protocol):
    def __init__(self,addr,clientConnections,port):
        self.addr = addr
        self.clientConnections = dict(clientConnections)
        self.port = port

    def connectionMade(self):
        '''gets address of client connection and adds to '''
        port = reactor.listenTCP(0,DataConnFactory(self))
        self.transport.write('port:'+str(port.getHost().port)+':port')
        self.clientConnections = CLIENT_CONNECTIONS
        self.clientConnections[str(port.getHost().port)] = 'need partner'

    def connectionLost(self,reason):
        print self.clientConnections
        global CLIENT_CONNECTIONS
        CLIENT_CONNECTIONS = self.clientConnections
        # try:
        #     commandConnFactory = CommandConnFactory(self.clientConnections)
        #     port = reactor.listenTCP(COMMAND_PORT,commandConnFactory)
        #     commandConnFactory.setPort(port)
        # except Exception, e:
        #     print 'error restarting listening connection'
        #     #traceback.print_exc()

    def removeConnection(self,portnum):
        global CLIENT_CONNECTIONS
        del(CLIENT_CONNECTIONS[portnum])

    def getPartner(self,portnum):
        for conn in CLIENT_CONNECTIONS:
            if CLIENT_CONNECTIONS[conn] == 'need partner' and conn != portnum:
                CLIENT_CONNECTIONS[conn] = 'partner:'+str(portnum)
                return conn

        return None
