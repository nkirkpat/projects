Names:	Nick Kirkpatrick
		Anthony Narin
Date:	4 May 2016

Connect Four:
=============

The final project is handed in with a zip file called connect4.zip.

How to use connect4.py:
  1) unzip the package and cd into the directory pygame
     $ unzip connect4.zip
     $ cd connect4
  2) On username@student00.cse.nd.edu, run the following commands from the directory connect4
     $ cd network
     $ python server.py
  3) Unzip the package connect4.zip into to different student machines, doesn't matter which and run:\
  	Example:
  	 On username@student01.cse.nd.edu:
  	 	$ unzip connect4.zip
  	 	$ cd connect4
  	 	$ python connect4.py
  	 On username@student02.cse.nd.edu:
  	 	$ unzip connect4.zip
  	 	$ cd connect4
  	 	$ python connect4.py
  4) The server should connect the two clients and they should be good to go. 
  5) Important to note that the game can only be played once. After completion, you should close both clients and then repeat step 3. The server code can still be running. 

How to Play:
============

When the image to the right of the connect four board tells you it is your turn, hover the mouse in the column of your choice, and drop the checker. Due to the latency if you are using ssh, I recommend holding the mouse until you see the checker begin to drop. Once the move is completed, the move will be mimmicked on your opponents board. After the move is completed, you may now make a move in the column of your choice. The winner of the game is the first player to connect 4 checkers in a row, either diagonally, horizontally, or vertically. An image will pop up in the bottom right of the screen signifying if you won or lost. Have Fun!

NOTES:
======
The host which you run the server on is determined in the two python files: networking/ClientConn.py and connect4.py.
	You will see a global variable below the imports called HOST. Whatever machine you run networking/server.py you must have running on the specified HOST. The default is student00.cse.nd.edu, so I recommend following the instruction above, or editing the HOST variable in both of those files.

NETWORKING - PORTION:
=====================

The networking portion of the connect 4 game has a simple job. It listens
on a specific port and then pairs connections between players who connect on
that port. However, for security I didn't think it was best to connect the
clients directly. Instead, I let the server function as a proxy.

I also wanted to maintain the server listening on the same port throughout so
clients could connect while others were playing.

I accomplished this by having clients connect to the server on one port,
triggering the server to listen on an OS decided port, and have the client
reconnect on that port. Then the server keeps track of the various data
connections and coordinates the transfer of information from client to client
when it receives data on the appropriate ports. 
