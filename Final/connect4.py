# Board Class

import random, copy, sys, pygame, time, os, signal
from pygame.locals import *

from network.ClientConn import *

BOARDWIDTH = 7  # how many spaces wide the board is
BOARDHEIGHT = 6 # how many spaces tall the board is

SPACESIZE = 50 # size of the tokens and individual board spaces in pixels
TURNSIZE = 300
WINHEIGHT = 200
WINWIDTH = 300

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BGCOLOR = BLACK
TEXTCOLOR = WHITE

RED = 'red'
BLUE = 'blue'
EMPTY = 'empty'
HOVER = 'hover'

BOARD_IMG = pygame.image.load('yellowBoard/empty.png')
BOARD_IMG = pygame.transform.smoothscale(BOARD_IMG, (SPACESIZE, SPACESIZE))
BLUE_IMG = pygame.image.load('yellowBoard/filledBY.png')
BLUE_IMG = pygame.transform.smoothscale(BLUE_IMG, (SPACESIZE, SPACESIZE))
RED_IMG = pygame.image.load('yellowBoard/filledRY.png')
RED_IMG = pygame.transform.smoothscale(RED_IMG, (SPACESIZE, SPACESIZE))
HOVER_IMG = pygame.image.load('yellowBoard/selectY.png')
HOVER_IMG = pygame.transform.smoothscale(HOVER_IMG, (SPACESIZE, SPACESIZE))
BLACK_IMG = pygame.image.load('yellowBoard/black.png')
BLACK_IMG = pygame.transform.smoothscale(BLACK_IMG, (SPACESIZE, SPACESIZE))
LOSE_IMG = pygame.image.load('lose.png')
LOSE_IMG = pygame.transform.smoothscale(LOSE_IMG, (WINWIDTH, WINHEIGHT))
WIN_IMG = pygame.image.load('win.png')
WIN_IMG = pygame.transform.smoothscale(WIN_IMG, (WINWIDTH, WINHEIGHT))
TURN_IMG = pygame.image.load('turn.png')
TURN_IMG = pygame.transform.smoothscale(TURN_IMG, (TURNSIZE, TURNSIZE))
WAIT_IMG = pygame.image.load('wait.png')
WAIT_IMG = pygame.transform.smoothscale(WAIT_IMG, (TURNSIZE, TURNSIZE))
AGAIN_IMG = pygame.image.load('again.png')
AGAIN_IMG = pygame.transform.smoothscale(AGAIN_IMG, (TURNSIZE, TURNSIZE))

FINAL_CHECKER = pygame.image.load('yellowBoard/selectY.png')
FINAL_CHECKER = pygame.transform.smoothscale(FINAL_CHECKER, (SPACESIZE, SPACESIZE))

IMAGES = {RED:RED_IMG,BLUE:BLUE_IMG,EMPTY:BOARD_IMG,HOVER:HOVER_IMG}

SERVER_HOST = 'student00.cse.nd.edu'
SERVER_PORT = 40079

class Board(pygame.sprite.Sprite):

	def __init__(self,gs=None):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.columns = []
		for x in range(BOARDWIDTH):
			col = []
			for y in range(BOARDHEIGHT):
				checker = Checker(self.gs)
				checker.rect.topleft = ((x * SPACESIZE), (y * SPACESIZE))
				col.append(checker)
			self.columns.append(col)
		self.drawBoard()

	def tick(self):
		for col in self.columns:
			if not self.isColFull(col):
		 		if self.mouseInColumn(col):
					if col[self.lowestRow(col)].image == IMAGES[EMPTY]:
						col[self.lowestRow(col)].hover()
				else:
					if col[self.lowestRow(col)].image == IMAGES[HOVER]:
						col[self.lowestRow(col)].unhover()

	def drawBoard(self):
		for col in self.columns:
			for y in range(BOARDHEIGHT):
				self.gs.screen.blit(col[y].image,col[y].rect)

	def isColFull(self,col):
		return col[0].color!=EMPTY

	def lowestRow(self,col):
		for y in range(BOARDHEIGHT-1,-1,-1):
			if col[y].color == EMPTY:
				return y
		return -1

	def mouseInColumn(self,col):
		for checker in col:
			if checker.rect.collidepoint(pygame.mouse.get_pos()):
				return True
		return False

	def dropToken(self,player):
		for idx, col in enumerate(self.columns):
			if not self.isColFull(col):
		 		if self.mouseInColumn(col):
		 			#initiate dropping effect
					with open(self.gs.file_name,'w+') as f:
						f.write('{0} {1}\n'.format(player.color,str(idx)))
					self.gs.orig = os.stat(self.gs.file_name)[8]
		 			lowIndex = self.lowestRow(col)
		 			for y in range(BOARDHEIGHT):
		 				if y == lowIndex:
		 					col[y].move(player)
		 					break
		 				else:
		 					col[y].move(player)
		 					time.sleep(.2)
		 					col[y].unhover()
		 					
	def dropOpponentsToken(self,player,col):
		lowIndex = self.lowestRow(self.columns[col])
		for y in range(BOARDHEIGHT):
			if y == lowIndex:
				self.columns[col][y].move(player)
				break
			else:
		 		self.columns[col][y].move(player)
		 		time.sleep(.2)
		 		self.columns[col][y].unhover()

	def win(self,checker):
		winners = []
		# check horizontal spaces
		for x in range(BOARDWIDTH - 3):
			for y in range(BOARDHEIGHT):
				if self.columns[x][y].color == checker and self.columns[x+1][y].color == checker and self.columns[x+2][y].color == checker and self.columns[x+3][y].color == checker:
					winners.append(self.columns[x][y])
					winners.append(self.columns[x+1][y])
					winners.append(self.columns[x+2][y])
					winners.append(self.columns[x+3][y])
		# check vertical spaces
		for x in range(BOARDWIDTH):
			for y in range(BOARDHEIGHT - 3):
				if self.columns[x][y].color == checker and self.columns[x][y+1].color == checker and self.columns[x][y+2].color == checker and self.columns[x][y+3].color == checker:
					winners.append(self.columns[x][y])
					winners.append(self.columns[x][y+1])
					winners.append(self.columns[x][y+2])		
					winners.append(self.columns[x][y+3])
		# check / diagonal spaces
		for x in range(BOARDWIDTH - 3):
			for y in range(3, BOARDHEIGHT):
				if self.columns[x][y].color == checker and self.columns[x+1][y-1].color == checker and self.columns[x+2][y-2].color == checker and self.columns[x+3][y-3].color == checker:
					winners.append(self.columns[x][y])
					winners.append(self.columns[x+1][y-1])
					winners.append(self.columns[x+2][y-2])
					winners.append(self.columns[x+3][y-3])
		# check \ diagonal spaces
		for x in range(BOARDWIDTH - 3):
			for y in range(BOARDHEIGHT - 3):
				if self.columns[x][y].color == checker and self.columns[x+1][y+1].color == checker and self.columns[x+2][y+2].color == checker and self.columns[x+3][y+3].color == checker:
					winners.append(self.columns[x][y])
					winners.append(self.columns[x+1][y+1])
					winners.append(self.columns[x+2][y+2])
					winners.append(self.columns[x+3][y+3])	
		return winners
 

class Checker(pygame.sprite.Sprite):
	def __init__(self,gs=None,color='empty'):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.rect = pygame.Rect(0, 0, SPACESIZE, SPACESIZE)
		self.color = color
		self.image = IMAGES[self.color]
	def hover(self):
		self.image = IMAGES[HOVER]
		self.gs.screen.blit(self.image,self.rect)
	def unhover(self):
		self.color = EMPTY
		self.image = IMAGES[self.color]
		self.gs.screen.blit(self.image,self.rect)
	def move(self,player):
		self.color = player.color
		self.image = IMAGES[self.color]
		self.gs.screen.blit(self.image,self.rect)
		pygame.display.flip()

class Player(pygame.sprite.Sprite):
	def __init__(self,gs=None,board=[],color='empty',isTurn=False):
		pygame.sprite.Sprite.__init__(self)
		self.gs = gs
		self.color = color
		self.isTurn = isTurn
		if self.isTurn:
			self.image = TURN_IMG
		else:
			self.image = WAIT_IMG
		self.rect = pygame.Rect(SPACESIZE*BOARDWIDTH,0,TURNSIZE,TURNSIZE)
		#self.gs.screen.blit(self.image,self.rect)
	def tick(self):
		if self.isTurn:
			if self.image == WAIT_IMG:
				self.image = TURN_IMG
				#self.gs.screen.blit(self.image,self.rect)
		else:
			if self.image == TURN_IMG:
				self.image = WAIT_IMG
				#self.gs.screen.blit(self.image,self.rect)
	def move(self):
		if self.isTurn:
			self.gs.board.dropToken(self)
		winners = self.gs.board.win(self.color)
		if winners:
			self.gs.winner = self.color
			for checker in winners:
				checker.hover()
				pygame.display.flip()
				time.sleep(.2)

	def win(self):
		self.endImage = WIN_IMG
		self.endRect = pygame.Rect(SPACESIZE*BOARDWIDTH,SPACESIZE*BOARDHEIGHT,WINWIDTH,WINHEIGHT)
		self.gs.screen.blit(self.endImage,self.endRect)
		self.image = AGAIN_IMG
		self.gs.screen.blit(self.endImage,self.endRect)
	def lose(self):
		self.endImage = LOSE_IMG
		self.endRect = pygame.Rect(SPACESIZE*BOARDWIDTH,SPACESIZE*BOARDHEIGHT,WINWIDTH,WINHEIGHT)
		self.gs.screen.blit(self.endImage,self.endRect)
		self.image = AGAIN_IMG
		self.gs.screen.blit(self.endImage,self.endRect)

class GameSpace:
	def __init__(self,file_name):
		self.file_name = file_name
		self.orig =  os.stat(self.file_name)[8]

	def main(self):
		# 1) basic initialization
		pygame.init()
		self.size = self.width, self.height = 640, 500
		self.screen = pygame.display.set_mode(self.size)
		pygame.display.set_caption('Connect 4!')
		# 2) set up game objects
		self.clock = pygame.time.Clock()
		self.board = Board(self)
		self.player = self.monitor_file()
		if self.player.color == RED:
			self.player2 = Player(self,self.board,BLUE,False)
		else:
			self.player2 = Player(self,self.board,RED,True)
		print 'you are player',self.player.color
		print 'your opponent is',self.player2.color
		self.winner = EMPTY
		# 3) start game loop
		self.orig = os.stat(self.file_name)[8]
		# 4) clock tick regulation (framerate)
		self.clock.tick(30)
		while 1:
				if not self.player.isTurn:
					m_time = os.stat(self.file_name)[8]
					if m_time > self.orig:
						with open(self.file_name,'r') as f:
							opponentsMove = f.read().split()
						print 'opponents move:',opponentsMove
						self.board.dropOpponentsToken(self.player2,int(opponentsMove[1]))
						winners = self.board.win(self.player2.color)
						if winners:
							self.winner = self.player2.color
							for checker in winners:
								checker.hover()
								pygame.display.flip()
								time.sleep(.2)
							self.player.lose()
						else:
							self.switch()
				# 5) this is where you would handle user inputs...
				for event in pygame.event.get():
					if event.type == pygame.QUIT:
						pygame.quit()
						exit()
				keystate = pygame.key.get_pressed()
				#if space bar is pressed or handling left and right arrows
				mousestate = pygame.mouse.get_pressed()
				if mousestate[0]:
					if self.winner == EMPTY:
						if self.player.isTurn:
							self.player.move()
							self.switch()
						if self.winner == self.player.color:
							self.player.win()
				# 6) send a tick to every game object!
				self.board.tick()
				self.player.tick()
				# 7) and finally, display the game objects
				self.screen.blit(self.player.image, self.player.rect)
				pygame.display.flip()
	def switch(self):
		temp = self.player.isTurn
		self.player.isTurn = (not temp)
		self.player2.isTurn = temp
	def monitor_file(self):
		color = ''
		while True:
			m_time = os.stat(self.file_name)[8]
			if m_time > self.orig:
				with open(self.file_name,'r') as f:
					color = f.read()
				break
			else:
				time.sleep(1)
		if color == RED:
			return Player(self,self.board,RED,True)
		else:
			return Player(self,self.board,BLUE,False)

if __name__ == '__main__':
    try:
        file_name = 'test.txt'
        pid = os.fork()

        if pid == 0:
            reactor.connectTCP(SERVER_HOST,SERVER_PORT,ClientConnFactory(file_name))
            reactor.run()
    #Parent
        else:
            gs = GameSpace(file_name)
            gs.main()

    finally:
        if pid != 0:
            os.kill(pid, signal.SIGKILL)
